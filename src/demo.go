package main

import (
	"fmt"
	"kilavo.com/kilavo-core/internal/api"
	"kilavo.com/kilavo-core/internal/config"
	crypto2 "kilavo.com/kilavo-core/internal/crypto"
	"kilavo.com/kilavo-core/internal/federation"
	"kilavo.com/kilavo-core/internal/networking"
	"kilavo.com/kilavo-core/internal/secretStore"
	"os"
	"os/signal"
)

func main(){

	nodes := map[string]*networking.Network{
		"node1" : nil,
		"node2" : nil,
		"node3" : nil,
	}

	admins := map[string]*api.AdminAPI{
		"node1" : nil,
		"node2" : nil,
		"node3" : nil,
	}

	oracleMap := map[string]string{}

	for node,_ := range nodes {
		fmt.Println("----------------------------------------------\n\n\t\tmaking Node: ",node,"\n\n")
		c1 := config.GetCustomConfig("test_files",node)
		secrets := secretStore.NewSecretStore(c1)
		crypto1 := crypto2.NewCrypto(c1,secrets)
		n1 := networking.NewNetwork(c1,crypto1)
		admin := api.NewAdminAPI(n1,secrets)
		fmt.Println("You Are listening on the following Addresses:")
		admins[node] = admin
		for _,n := range n1.GetAddresses() {
			fmt.Println("\t*",n)
		}
		oracleMap[n1.GetAddresses()[1]] = "testgroup"
		nodes[node] = n1
	}


	oracle := federation.TestFedOracle{
		oracleMap,
	}


	for node,n1 := range nodes {
		fmt.Println("----------------------------------------------\n\n\t\tchecking federation instance: ",node,"\n\n")
		fmt.Println("Your federations:")
		for _,n := range oracle.GetFederations(n1.Cid()) {
			fmt.Println("\t*",n)
		}

	}

	for node,n1 := range nodes {
		fmt.Println("----------------------------------------------\n\n\t\tmaking federation instance: ",node,"\n\n")

		for _,n := range oracle.GetFederations(n1.Cid()) {
			f := federation.NewFederation(n,n1,&oracle)
			admins[node].AddFederation(f)
			fmt.Println(f)
		}

	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<- c

}
