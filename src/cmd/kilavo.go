package main

import (
	"fmt"
	"github.com/spf13/cobra"
	"kilavo.com/kilavo-core/cmd/common"
	"kilavo.com/kilavo-core/cmd/config"
	"os"
)


var rootCmd = &cobra.Command{
	Use:   "kilavo",
	Short: "Kilavo is a decentralized infrastructure provider for Tezos",
	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
	},
}

var runCmd = &cobra.Command{
	Use:   "daemon",
	Short: "Start Kilavo Daemon",
	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
	},
}


func main(){
	s1 := ""
	s2 := ""
	ch := common.CmdHelper{
		&s1,&s2,
	}

	rootCmd.PersistentFlags().StringVarP(ch.ConfigName, "config", "c", "kilavo", "config name ( without .yml etc )")
	rootCmd.PersistentFlags().StringVarP(ch.ConfigDir, "configdir", "d", "", "config directory")

	rootCmd.AddCommand(runCmd)
	rootCmd.AddCommand(config.GetSecretsCommand(ch))
	rootCmd.AddCommand(config.GetSwarmCommand(ch))


	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
