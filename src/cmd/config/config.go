package config

import (
	"fmt"
	"github.com/spf13/cobra"
	"kilavo.com/kilavo-core/cmd/common"
	crypto2 "kilavo.com/kilavo-core/internal/crypto"
	"kilavo.com/kilavo-core/internal/networking"
	"kilavo.com/kilavo-core/internal/secretStore"
)

func GetSwarmCommand(f common.CmdHelper) *cobra.Command {
	var swarm = &cobra.Command{
		Use:   "swarm",
		Short: "p2p Network",
	}



	var address = &cobra.Command{
		Use:   "id",
		Short: "show your local ID",
		Run: func(cmd *cobra.Command, args []string) {
			c := f.GetConfig()
			secrets := secretStore.NewSecretStore(c)
			crypto := crypto2.NewCrypto(c,secrets)
			n := networking.NewNetwork(c,crypto)
			fmt.Println("Your Addresses are:")
			for _,nn := range n.GetAddresses() {
				fmt.Println("\t *", nn)
			}
		},
	}

	swarm.AddCommand(address)
	return swarm
}

