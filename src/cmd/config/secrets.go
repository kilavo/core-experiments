package config

import (
	"fmt"
	"github.com/spf13/cobra"
	"kilavo.com/kilavo-core/cmd/common"
	crypto2 "kilavo.com/kilavo-core/internal/crypto"
	"kilavo.com/kilavo-core/internal/secretStore"
)

func GetSecretsCommand(f common.CmdHelper) *cobra.Command {
	var secrets = &cobra.Command{
		Use:   "config",
		Short: "manage sensitive data",
	}

	var idkeys = &cobra.Command{
		Use:   "identity",
		Short: "show admin Keys",
		Run: func(cmd *cobra.Command, args []string) {
			c := f.GetConfig()
			secrets := secretStore.NewSecretStore(c)
			crypto := crypto2.NewCrypto(c,secrets)
			fmt.Println("\nPublic Key:\n")
			fmt.Println(crypto.GetPublicKeyString("identity"))
			fmt.Println("\nPrivate Key:\n")
			fmt.Println(crypto.GetPrivateKeyString("identity"))
		},
	}

	var admins = &cobra.Command{
		Use:   "admins",
		Short: "manage Admin ids",
	}

	var list = &cobra.Command{
		Use:   "list",
		Short: "List Admin Keys",
		Run: func(cmd *cobra.Command, args []string) {
			c := f.GetConfig()
			secrets := secretStore.NewSecretStore(c)
			ids := secrets.GetAdminIDs()
			for _,a := range ids {
				fmt.Println("\t*",a)
			}
		},
	}

	var add = &cobra.Command{
		Use:   "add",
		Short: "Add Admin Key",
		Run: func(cmd *cobra.Command, args []string) {
			c := f.GetConfig()
			secrets := secretStore.NewSecretStore(c)
			if len(args) == 0 {
				fmt.Println("Please provide a peer-id cid")
			} else {
				// TODO parse & check
				secrets.AddAdminId(args[0])
			}
		},
	}

	var block = &cobra.Command{
		Use:   "block",
		Short: "Block Admin Key",
		Run: func(cmd *cobra.Command, args []string) {
			c := f.GetConfig()
			secrets := secretStore.NewSecretStore(c)
			if len(args) == 0 {
				fmt.Println("Please provide a peer-id cid")
			} else {
				// TODO parse & check
				secrets.BlockAdminId(args[0])
			}
		},
	}

	admins.AddCommand(list)
	admins.AddCommand(add)
	admins.AddCommand(block)
	secrets.AddCommand(admins)
	secrets.AddCommand(idkeys)
	return secrets
}

