package common

import "kilavo.com/kilavo-core/internal/config"

type CmdHelper struct {
	ConfigDir *string
	ConfigName *string
}

func sp(s string) *string{
	return &s
}

func (c *CmdHelper) GetConfig() *config.Config {
	if *c.ConfigName == "" {
		c.ConfigName = sp("kilavo")
	}
	if c.ConfigDir == nil {
		return config.GetConfig(*c.ConfigName)
	} else {
		return config.GetCustomConfig(*c.ConfigDir,*c.ConfigName)
	}
	return nil
}