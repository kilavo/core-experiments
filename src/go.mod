module kilavo.com/kilavo-core

go 1.15

require (
	github.com/boltdb/bolt v1.3.1
	github.com/c9s/goprocinfo v0.0.0-20210130143923-c95fcf8c64a8
	github.com/google/uuid v1.2.0
	github.com/hashicorp/raft v1.1.1
	github.com/hashicorp/raft-boltdb v0.0.0-20191021154308-4207f1bf0617
	github.com/libp2p/go-libp2p v0.13.0
	github.com/libp2p/go-libp2p-consensus v0.0.1
	github.com/libp2p/go-libp2p-core v0.8.5
	github.com/libp2p/go-libp2p-mplex v0.4.1
	github.com/libp2p/go-libp2p-pubsub v0.4.1
	github.com/libp2p/go-libp2p-raft v0.1.7
	github.com/libp2p/go-libp2p-webrtc-direct v0.0.0-20201219114432-56b02029fbb8
	github.com/libp2p/go-tcp-transport v0.2.1
	github.com/libp2p/go-ws-transport v0.4.0
	github.com/moby/locker v1.0.1
	github.com/multiformats/go-multiaddr v0.3.1
	github.com/pion/webrtc/v2 v2.2.26
	github.com/shirou/gopsutil/v3 v3.21.2
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	github.com/whyrusleeping/mdns v0.0.0-20190826153040-b9b60ed33aa9
	go.opencensus.io v0.22.4
)
