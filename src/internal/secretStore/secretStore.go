package secretStore

import (
	"fmt"
	"github.com/boltdb/bolt"
	"kilavo.com/kilavo-core/internal/config"
	"log"
	"os"
	"time"
)

type SecretsStore struct {
	db *bolt.DB
}

func NewSecretStore(c *config.Config) *SecretsStore {
	if _, err := os.Stat(c.Storage.Secrets.Bolt.File); os.IsNotExist(err) {
		_,err := os.Create(c.Storage.Secrets.Bolt.File)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	db, err := bolt.Open(c.Storage.Secrets.Bolt.File, 0644, &bolt.Options{Timeout: 100 * time.Millisecond})
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	s := SecretsStore{}
	db.Update(func(tx *bolt.Tx) error {
		tx.CreateBucket([]byte("kv"))
		return nil
	})
	db.Update(func(tx *bolt.Tx) error {
		tx.CreateBucket([]byte("admin_ids"))
		return nil
	})
	s.db = db
	return &s
}


func (s *SecretsStore) Save(key,val string) error {
	return s.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("kv"))
		err := b.Put([]byte(key), []byte(val))
		return err
	})
}

func (s *SecretsStore) Get(key string) string {
	res := ""
	s.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("kv"))
		v := b.Get([]byte(key))
		res = string(v)
		return nil
	})
	return res
}

func (s *SecretsStore) AddAdminId(id string) string {
	res := ""
	s.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("admin_ids"))
		return b.Put([]byte(id),[]byte("accepted"))
	})
	return res
}

func (s *SecretsStore) BlockAdminId(id string) string {
	res := ""
	s.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("admin_ids"))
		return b.Put([]byte(id),[]byte("blocked"))
	})
	return res
}



func (s *SecretsStore) GetAdminIDs() []string {
	res := []string{}
	s.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("admin_ids"))
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			if string(v) == "accepted" {
				res = append(res, string(k))
			}
		}
		return nil
	})
	return res
}

func (s *SecretsStore) IsAdmin(cid string) bool{
	for _,a := range s.GetAdminIDs() {
		if cid == a { return true }
	}
	return false
}