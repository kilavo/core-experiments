package tezosRPC

import (
	"errors"
	"io/ioutil"
	"net/http"
)

type SimpleRPC struct {
	Node string
	Name string
}


func NewRPC(node, name string) *SimpleRPC{
	s := SimpleRPC{
		Node: node,
		Name: name,
	}
	return &s
}

func (rpc *SimpleRPC) GET(url string) ([]byte,error) {
	resp, err := http.Get(rpc.Node + url)
	if err != nil {
		return []byte{},errors.New("Error reaching Tezos node: " + rpc.Node)
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return []byte{},errors.New("Unacepptable Status Code: " + resp.Status)
	}
	b,_ := ioutil.ReadAll(resp.Body)
	return b,err
}