package tezosRPC

import "kilavo.com/kilavo-core/internal/config"


type TezosRPCProvider struct {
	providers map[string]*SimpleRPC
}

func NewRPCProvider(c *config.Config) *TezosRPCProvider {
	r := &TezosRPCProvider{
		providers: map[string]*SimpleRPC{},
	}
	for _,n := range c.TezosNets {
		n := NewRPC(n.Node,n.Name)
		r.providers[n.Name] = n
	}
	return r
}

