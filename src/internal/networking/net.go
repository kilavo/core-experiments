package networking

import (
	"context"
	"fmt"
	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/peerstore"
	pubsub "github.com/libp2p/go-libp2p-pubsub"
	"github.com/libp2p/go-libp2p/p2p/discovery"
	"github.com/libp2p/go-tcp-transport"
	ws "github.com/libp2p/go-ws-transport"
	"github.com/moby/locker"
	ma "github.com/multiformats/go-multiaddr"
	"kilavo.com/kilavo-core/internal/config"
	"kilavo.com/kilavo-core/internal/crypto"
	"os"
	"strconv"
	"strings"
	"time"
)

type Network struct {
	context context.Context
	host host.Host
	pubsub *pubsub.PubSub
	topics map[string]*pubsub.Topic
	locks *locker.Locker
	crypto *crypto.Crypto
}


func NewNetwork(c *config.Config,crypto *crypto.Crypto) *Network{
	n := Network{
		context: context.Background(),
		locks: locker.New(),
		topics: map[string]*pubsub.Topic{},
	}
	opts := []libp2p.Option{}
	opts = append(opts, libp2p.Identity(crypto.GetPrivateKey()))
	addresses := []string{}
	if c.Networking.TCP.Enable {
		opts = append(opts, libp2p.Transport(tcp.NewTCPTransport))
		addresses = append(addresses, "/ip4/0.0.0.0/tcp/" + strconv.Itoa(c.Networking.TCP.Port))
	}
	if c.Networking.Websockets.Enable {
		opts = append(opts,libp2p.Transport(ws.New))
		addresses = append(addresses, "/ip4/0.0.0.0/tcp/"+strconv.Itoa(c.Networking.Websockets.Port)+"/ws")
	}
	for _,a := range addresses {
		sourceMultiAddr, _ := ma.NewMultiaddr(a)
		opts = append(opts, libp2p.ListenAddrs(sourceMultiAddr))
	}



	l,err := libp2p.New(n.context,opts...)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	n.host = l
	pubsub,e := pubsub.NewGossipSub(n.context,l)
	if e != nil {
		fmt.Println(e)
		os.Exit(1)
	}
	n.pubsub = pubsub
	_,err = discovery.NewMdnsService(context.Background(),n.host,1*time.Second,"kialvo")
	if err != nil {
		fmt.Println("DISCOVERY ERROR: ", err)
	}
	return &n
}

func (n *Network) AddPeer(addr string){
	maddr,err := ma.NewMultiaddr(addr)
	if err != nil {
		fmt.Println(err)
	}
	id,err := peer.AddrInfoFromP2pAddr(maddr)
	if err != nil {
		fmt.Println(err)
	}
	n.host.Peerstore().AddAddrs(id.ID,id.Addrs,peerstore.PermanentAddrTTL)
}

func (n *Network) Cid() string{
	 return n.host.ID().String()
}

func (n *Network) GetHost() host.Host {
	return n.host
}

func (n *Network) GetAddresses() []string{
	hostAddr, _ := ma.NewMultiaddr(fmt.Sprintf("/ipfs/%s", n.host.ID().Pretty()))
	m := n.host.Addrs()
	r := []string{}
	for _,a := range m {
		r = append(r, a.Encapsulate(hostAddr).String())
	}
	return r
}

func (n *Network) PublishString(topic, msg string) error {
	topic = strings.ToLower(topic)
	topic = strings.Trim(topic," ")
	if val,ok := n.topics[topic]; ok {
		return val.Publish(n.context,[]byte(msg))
	} else {
		n.locks.Lock("topics")
		defer n.locks.Unlock("topics")
		t,e := n.pubsub.Join(topic)
		if e != nil {
			return e
		}
		n.topics[topic] = t
		return t.Publish(n.context,[]byte(msg))
	}
}

func (n *Network) PublishBytes(topic string, msg []byte) error {
	topic = strings.ToLower(topic)
	topic = strings.Trim(topic," ")
	if val,ok := n.topics[topic]; ok {
		return val.Publish(n.context,msg)
	} else {
		n.locks.Lock("topics")
		defer n.locks.Unlock("topics")
		t,e := n.pubsub.Join(topic)
		if e != nil {
			return e
		}
		n.topics[topic] = t
		return t.Publish(n.context,msg)
	}
}