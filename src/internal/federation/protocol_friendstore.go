package federation

import (
	"errors"
	"strings"
	"sync"
	"time"
)

type Friend struct {
	CID string
	sendChannel chan FederationDirectMessage
	Status string // OK / unhealthy
	LastPing time.Time
}

type FriendStore struct {
	friends map[string]*Friend // cid to string
	pings map[string]time.Time
	latency map[string]time.Duration
	messages chan *FederationDirectMessage
	m *sync.Mutex
}


func NewFriendStore() *FriendStore {
	f := FriendStore{}
	f.friends = map[string]*Friend{}
	f.pings = map[string]time.Time{}
	f.latency = map[string]time.Duration{}
	f.m = &sync.Mutex{}
	go f.CheckStatus()
	return &f
}

func (f *FriendStore) CheckStatus(){
	for {
		f.m.Lock()
		for _,fr := range f.friends {
			if val,ok := f.pings[fr.CID]; !ok {
				fr.Status = "unhealthy"
			} else {
				if val.After(time.Now().Add(-2*time.Second)) {
					f.latency[fr.CID] = time.Now().Sub(val)
					fr.Status = "ok"
				} else {
					fr.Status = "unhealthy"
				}
			}
		}
		f.m.Unlock()
		time.Sleep(1*time.Second)
	}
}


func (friend *Friend) SendMessage(msg FederationDirectMessage) error {
	if friend.Status == "ok" {
		friend.sendChannel <- msg
		return nil
	}
	return errors.New("Tried to send to a peer that is not healthy")
}

func (f *FriendStore) Add(fr *Friend) {
	f.friends[fr.CID] = fr
}

func (f *FriendStore) SetPings(fr *Friend,time time.Time) {
	f.m.Lock()
	defer f.m.Unlock()
	f.pings[fr.CID] = time
}

/*
 * May return nil
 */
func (f *FriendStore) Get(cid string) *Friend{
	f.m.Lock()
	defer f.m.Unlock()
	for val,f := range f.friends {
		if f.Status == "ok" && val == cid || strings.Contains(cid,val) {
			return f
		}
	}
	return nil
}

func (f *FriendStore) GetLatencies() map[string]time.Duration {
	f.m.Lock()
	defer f.m.Unlock()
	return f.latency
}