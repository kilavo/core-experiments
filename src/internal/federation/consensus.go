package federation

import (
	"errors"
	"fmt"
	"github.com/hashicorp/raft"
	consensus "github.com/libp2p/go-libp2p-consensus"
	libp2praft "github.com/libp2p/go-libp2p-raft"
	"io/ioutil"
	"kilavo.com/kilavo-core/internal/networking"
	"log"
	"time"
)


type Consensus struct {
	cid string
	consensus consensus.OpLogConsensus
	state *ConsensusState
	raft *raft.Raft
	baseOp *ConsensusChange
	stateChanges map[string]func(string, *ConsensusState)
}


func NewConsensus(n *networking.Network, peers []string) *Consensus {
	r := Consensus{}
	r.cid = n.Cid()
	state := ConsensusState{
		Config: FederationConfig{},
		Log: []FederationLog{},
	}
	r.state = &state
	baseOp := &ConsensusChange{}
	r.baseOp = baseOp
	consensus := libp2praft.NewOpLog(&state,baseOp)
	transport,err := libp2praft.NewLibp2pTransport(n.GetHost(), time.Minute)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	peers = append(peers, n.Cid())
	servers := make([]raft.Server, 0)
	for _,p := range peers {
		servers = append(servers, raft.Server{
			Suffrage: raft.Voter,
			ID:       raft.ServerID(p),
			Address:  raft.ServerAddress(p),
		})
	}

	serversCfg := raft.Configuration{servers}
	config := raft.DefaultConfig()
	config.LogOutput = ioutil.Discard
	config.Logger = nil
	config.LocalID = raft.ServerID(n.Cid())

	// poc is in-memory....should be stored on disk of course
	snapshots := raft.NewInmemSnapshotStore()
	logStore := raft.NewInmemStore()
	raft.BootstrapCluster(config, logStore, logStore, snapshots, transport, serversCfg.Clone())

	raft, err := raft.NewRaft(config, consensus.FSM(), logStore, logStore, snapshots, transport)
	if err != nil {
		log.Fatal(err)
	}
	r.raft = raft

	actor := libp2praft.NewActor(raft)
	consensus.SetActor(actor)

	// wait some time for election done
	time.Sleep(5*time.Second)

	return &r

}


func (f *Consensus) CommitLocal(op *ConsensusChange) error{
	if f.cid != string(f.raft.Leader()) {
		return errors.New("I am not the leader")
	}
	_, err := f.consensus.CommitOp(op)
	return err
}

func (f *Consensus) GetLeader() string {
	return string(f.raft.Leader())
}