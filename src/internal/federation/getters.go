package federation


func (f *Federation) GetPeerLatencies() map[string]int64{
	res := map[string]int64{}
	l := f.Friends.GetLatencies()
	for a,b := range l {
		res[a] = b.Milliseconds()
	}
	return res
}

func (f *Federation) GetPeers() []string{
	res := []string{}
	l := f.Friends.GetLatencies()
	for a,_ := range l {
		res = append(res, a)
	}
	return res
}