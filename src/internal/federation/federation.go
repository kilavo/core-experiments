package federation

import (
	"kilavo.com/kilavo-core/internal/networking"
	"time"
)

type Federation struct {
	Name string
	Networking *networking.Network
	Peers []string //
	oracle FederationOracle
	Friends *FriendStore // stores our federation members
	consensus *Consensus
}

type State struct {
	Peers map[string]int // cid -> PEER_STATE_(UN)HEALTHY
}

func NewFederation(name string, n *networking.Network, fo FederationOracle) *Federation {
	f := Federation{}
	f.Name = name
	f.Networking = n
	f.oracle = fo
	go f.join()
	go f.runFriendProtocol()
	go f.bootstrapConsensus()
	return &f
}


func (f *Federation) bootstrapConsensus(){
	time.Sleep(5*time.Second)
	f.consensus = NewConsensus(f.Networking,f.GetPeers())
}


func (f *Federation) join(){
	for {
		time.Sleep(1*time.Second)
		peers := f.oracle.GetPeers(f.Networking.Cid(),f.Name)
		for _,p := range peers {
			f.Networking.AddPeer(p)
		}
	}
}