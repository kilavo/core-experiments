package federation

import (
	consensus "github.com/libp2p/go-libp2p-consensus"
	"time"
)

const (
	PEER_STATE_UNHELATHY = iota
	PEER_STATE_HEALTHY = iota
)

type IndexerConfig struct {
	IndexerName string
	Image string
	Source string
	SubscribedTo []string // topics
	State string
	SyncStatus uint // blockheight upto which this is synced
}


type UpstreamConfig struct{
	Name string
	Addresses []string
}

type FederationConfig struct{
	Network string
	Upstream struct{
		Name string
		Addresses []string
	}
}

type FederationLog struct {
	Timestamp time.Time
	Message string
	Tags []string
}

type FederationEvent struct {
	Timestamp time.Time
	Payload []string
	Tags []string
	Topic string
}

type ConsensusState struct {
	Config FederationConfig
	Log []FederationLog
}

/*
 *
 */
type ConsensusChange struct {
	Tag string
	Method string
	Payload []byte
}

func (cc *ConsensusChange) ApplyTo(state consensus.State) (consensus.State,error) {
	return state,nil
}