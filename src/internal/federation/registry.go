package federation

import (
	"kilavo.com/kilavo-core/internal"
	"strings"
)

type FederationOracle interface {
	GetFederations(string) []string
	GetPeers(string,string) []string
}


type TestFedOracle struct {
	// CID - fed-name
	Members map[string]string
}

func (tfo *TestFedOracle) GetFederations(cid string) []string {
	res := []string{}
	for a,b := range tfo.Members {
		if strings.Contains(a,cid) {
			res = internal.AppendIfMissing(res,b)
		}
	}
	return res
}


func (tfo *TestFedOracle) GetPeers(cid string,federation string) []string {
	res := []string{}
	for oaddr,ofname := range tfo.Members {
		if !strings.Contains(oaddr,cid) && ofname == federation{
			res = internal.AppendIfMissing(res,oaddr)
		}
	}
	return res
}