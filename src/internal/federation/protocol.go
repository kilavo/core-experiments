package federation

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	core "github.com/libp2p/go-libp2p-core"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/multiformats/go-multiaddr"
	"math/rand"
	"strings"
	"time"
)

type FederationDirectMessage struct {
	ID      string
	Type    string
	Payload []byte
	Topic   string
}

/*
* Custom libp2p for feredation members to talk directly to each other
 */

func (f *Federation) runFriendProtocol() {
	f.Friends = NewFriendStore()
	pid := "/kilavo/federation/" + strings.ToLower(f.Name) + "/v0.0.1"
	f.Networking.GetHost().SetStreamHandler(core.ProtocolID(pid), f.streamhandler)
	go f.connectFriends()
}

func (f *Federation) connectFriends() {
	myself := f.Networking.Cid()
	sleeper := time.Duration(rand.Intn(10)) * time.Millisecond
	for {
		time.Sleep(sleeper)
		for _, addr := range f.oracle.GetPeers(myself, f.Name) {
			maddr, _ := multiaddr.NewMultiaddr(addr)
			info, _ := peer.AddrInfoFromP2pAddr(maddr)
			p := info.ID.String()
			if strings.Compare(p, myself) == -1 { // only need one connection
				if f.Friends.Get(p) == nil {
					f.callFriend(p)
				}
			}
		}
	}
}
func (f *Federation) callFriend(addr string) {
	p, _ := peer.Decode(addr)
	pid := "/kilavo/federation/" + strings.ToLower(f.Name) + "/v0.0.1"
	for {
		s, err := f.Networking.GetHost().NewStream(context.Background(), p, core.ProtocolID(pid))
		if err == nil {
			go f.streamhandler(s)
			break
		}
		fmt.Println(err)
		time.Sleep(3 * time.Second)
	}
}

func (f *Federation) streamhandler(s core.Stream) {
	cid := s.Conn().RemotePeer().String()
	fs := Friend{}
	fs.CID = cid
	fs.Status = "connecting"
	fs.sendChannel = make(chan FederationDirectMessage, 10)
	peers := f.oracle.GetPeers(f.Networking.Cid(), f.Name)
	notfriend := true
	for _, p := range peers {
		if strings.Contains(p, cid) {
			fmt.Println("Stream established: ", f.Networking.Cid(), "<->",cid)
			go f.handleFriendIncomming(s, cid)
			go f.handleFriendWriter(fs.sendChannel, s)
			go f.sendPings(s)
			f.Friends.Add(&fs)
			notfriend = false
		}
	}
	if notfriend {
		fmt.Println("WARN: " + cid + " tried to connect to federation internal protocol")
	}
}

func (f *Federation) handleFriendWriter(msgs chan FederationDirectMessage, s core.Stream) {
	for {
		msg := <-msgs
		b, _ := json.Marshal(msg)
		b = append(b, '\n')
		s.Write(b)
	}
}

func (f *Federation) sendPings(s core.Stream) {
	for {
		timestamp, _ := time.Now().MarshalBinary()
		msg := FederationDirectMessage{
			Payload: timestamp,
			Topic:   "LatencyCheck",
			ID:      uuid.New().String(),
			Type:    "system",
		}
		b, _ := json.Marshal(msg)
		b = append(b, '\n')
		_, e := s.Write(b)
		if e != nil {
			fmt.Println(e)
		}
		time.Sleep(1 * time.Second)
	}
}

func (f *Federation) handleFriendIncomming(s core.Stream, cid string) {
	for {
		scanner := bufio.NewScanner(bufio.NewReader(s))
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			b := scanner.Bytes()
			req := FederationDirectMessage{}
			err := json.Unmarshal(b, &req)
			if err != nil {
				fmt.Println("Error on federation internal protocol: Got strange request")
			} else {
				if req.Topic == "LatencyCheck" {
					t := time.Now()
					t.UnmarshalBinary(req.Payload)
					friend := f.Friends.Get(cid)
					f.Friends.SetPings(friend, t)
				} else {
					f.handleMessage(req, f.Friends.Get(cid).sendChannel)
				}

			}
		}
	}
}

func (f *Federation) handleMessage(msg FederationDirectMessage, answerchan chan FederationDirectMessage) {
	if msg.Type == "consensus" {

	}
}
