package api

import (
	"encoding/json"
	"kilavo.com/kilavo-core/internal/networking"
)

func RegisterNetInfo(api *AdminAPI, network *networking.Network) {
	api.RegisterMethod("network_ma",NetIds(network))
}


func NetIds(n *networking.Network) RPCMethod{
	return func(args string) *SimpleRpcAnswer {
		s := &SimpleRpcAnswer{}
		s.Data = "Error getting Network Info"
		Ids := n.GetAddresses()
		b,_ := json.Marshal(Ids)
		s.Data = string(b)
		return s
	}
}