package api

import (
	"encoding/json"
	linuxproc "github.com/c9s/goprocinfo/linux"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"runtime"
	"github.com/shirou/gopsutil/v3/mem"
)

func RegisterNodeInfo(api *AdminAPI){
	api.RegisterMethod("public_ip",GetPublicIp)
	api.RegisterMethod("host_cpu_stats",GetCPUStat)
	api.RegisterMethod("kila_mem_stats",GetKilavoMemStat)
	api.RegisterMethod("host_mem_stats",GetMEMStat)
	api.RegisterMethod("hostname",GetHostname)
}


func GetPublicIp(args string) *SimpleRpcAnswer {
	s := &SimpleRpcAnswer{}
	s.Data = "Error: could not get IP"
	url := "https://api.ipify.org?format=text"
	resp, err := http.Get(url)
	if err != nil {
		return s
	}
	defer resp.Body.Close()
	ip, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return s
	}
	s.Data = string(ip)
	return s
}

func GetCPUStat(args string) *SimpleRpcAnswer {
	s := &SimpleRpcAnswer{}
	s.Data = "Error: could not get Data"
	stat, err := linuxproc.ReadStat("/proc/stat")
	if err != nil {
		log.Fatal("stat read fail")
	}
	b,_ := json.Marshal(stat)
	s.Data = string(b)
	return s
}

func GetMEMStat(args string) *SimpleRpcAnswer {
	s := &SimpleRpcAnswer{}
	s.Data = "Error: could not get Data"
	v, err := mem.VirtualMemory()
	if err != nil {
		return s
	}
	b,_ := json.Marshal(v)
	s.Data = string(b)
	return s
}

func GetKilavoMemStat(args string) *SimpleRpcAnswer {
	s := &SimpleRpcAnswer{}
	s.Data = "Error: could not get Data"
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	m.Alloc = bToMb(m.Alloc)
	m.TotalAlloc = bToMb(m.TotalAlloc)
	m.Sys = bToMb(m.Sys)
	b,err := json.Marshal(m)
	if err != nil {
		return s
	}
	s.Data = string(b)
	return s
}

func GetHostname(args string) *SimpleRpcAnswer {
	s := &SimpleRpcAnswer{}
	s.Data = "Error: could not get Data"
	name,err := os.Hostname()
	if err != nil {
		return s
	}
	s.Data = name
	return s
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
