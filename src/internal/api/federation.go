package api

import (
	"encoding/json"
	"kilavo.com/kilavo-core/internal/federation"
	"strings"
)

func RegisterFederation(api *AdminAPI, f *federation.Federation) {
	api.RegisterMethod("federation_" + strings.ToLower(f.Name) + "_latencies",FederationLatencies(f))
}


func FederationLatencies(f *federation.Federation) RPCMethod{
	return func(args string) *SimpleRpcAnswer {
		s := &SimpleRpcAnswer{}
		s.Data = "Error getting Network Info"
		lats := f.GetPeerLatencies()
		b,_ := json.Marshal(lats)
		s.Data = string(b)
		return s
	}
}