package api

import (
	"bufio"
	"encoding/json"
	"fmt"
	core "github.com/libp2p/go-libp2p-core"
	"kilavo.com/kilavo-core/internal/federation"
	"kilavo.com/kilavo-core/internal/networking"
	"kilavo.com/kilavo-core/internal/secretStore"
	"time"
)

type AdminAPI struct {
	net    *networking.Network
	secret *secretStore.SecretsStore
	methods map[string]RPCMethod
	subs map[string]core.Stream
	federations []*federation.Federation
	topics map[string]RPCMethod
}

type SimpleRpc struct {
	Request string `json:"req"`
	Args    string `json:"args"`
	Id      string `json:"id"`
}

type SimpleRpcAnswer struct {
	ID   string
	Data string
}

type SimpleSubscription struct {
	Topic   string
	Data string
}

type RPCMethod func(string) *SimpleRpcAnswer

func NewAdminAPI(n *networking.Network, s *secretStore.SecretsStore) *AdminAPI {
	a := AdminAPI{
		net:    n,
		secret: s,
	}
	a.methods = map[string]RPCMethod{
		"time": a.TimeHandler,
		"subtest": a.SubsTest,
		"federations": a.GetFederations,
	}
	a.topics = map[string]RPCMethod{
		"federations": a.GetFederations,
	}
	a.federations = []*federation.Federation{}
	a.subs = map[string]core.Stream{}
	RegisterNodeInfo(&a)
	RegisterNetInfo(&a,n)
	go a.run()
	return &a
}

func (a *AdminAPI) AddFederation(f *federation.Federation){
	RegisterFederation(a,f)
	a.federations = append(a.federations, f)
	a.SendDataUpdate("federations",a.GetFederations)
}

func (a *AdminAPI) RegisterMethod(method string, f RPCMethod){
	a.methods[method] = f
}

func (a *AdminAPI) run() {
	a.net.GetHost().SetStreamHandler("/kilavo/admin/0.0.1", a.streamhandler)
}

func (a *AdminAPI) streamhandler(s core.Stream) {
	friend := s.Conn().RemotePeer().String()
	if a.secret.IsAdmin(friend) {
		scanner := bufio.NewScanner(bufio.NewReader(s))
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			b := scanner.Bytes()
			req := SimpleRpc{}
			err := json.Unmarshal(b, &req)
			if err != nil {
				fmt.Println("Error on protocol /kilavo/admin/0.0.1: Got strange request")
			} else {
				if val,ok := a.methods[req.Request]; ok {
					go func() {
						res := val(req.Args)
						if res != nil {
							res.ID = req.Id
							jb,_ := json.Marshal(res)
							s.Write(jb)
						}
					}()
				} else {
					if req.Request == "subscribe" {
						a.subs[req.Args] = s
						for k,m := range a.topics {
							if k == req.Args {
								a.SendDataUpdate(k,m)
							}
						}
					} else {
						fmt.Println("Unknown Method:", req.Request)
					}
				}
			}
		}
	} else {
		fmt.Println("WARN", friend, "tried to upgrade to /kilavo/admin protocol but is not an admin...")
	}
}

/**********************************************
 * Subscriptions
 */

func (a *AdminAPI) SendUpdate(subs, data string){
	for a,s := range a.subs {
		if a == subs {
			d := SimpleSubscription{
				Topic: subs,
				Data: data,
			}
			b,_ := json.Marshal(d)
			s.Write(b)
		}
	}
}

func (a *AdminAPI) SendDataUpdate(name string, f RPCMethod){
	res := f("data_update")
	a.SendUpdate(name,res.Data)
}

/***********************************************
 * RPC Handlers
 */

func ( a*AdminAPI) TimeHandler(args string) *SimpleRpcAnswer{
	return &SimpleRpcAnswer{
		Data: time.Now().String(),
	}
}

func ( a*AdminAPI) SubsTest(args string) *SimpleRpcAnswer{
	a.SendUpdate(args, "Test Publish")
	return nil
}

func (a *AdminAPI) GetFederations(args string) *SimpleRpcAnswer{
	fed_names := []string{}
	for _,f := range a.federations {
		fed_names = append(fed_names, f.Name)
	}
	jsonb,_ := json.Marshal(fed_names)
	return &SimpleRpcAnswer{
		Data: string(jsonb),
	}
}


/***********************************************
 * Utils
 */


// dropCR drops a terminal \r from the data.
func dropCR(data []byte) []byte {
	if len(data) > 0 && data[len(data)-1] == '\r' {
		return data[0 : len(data)-1]
	}
	return data
}

