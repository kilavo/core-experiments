package crypto

import (
	crand "crypto/rand"
	"fmt"
	"github.com/libp2p/go-libp2p-core/crypto"
	"kilavo.com/kilavo-core/internal/config"
	"kilavo.com/kilavo-core/internal/secretStore"
	"os"
	b64 "encoding/base64"
)

type Crypto struct {
	priv crypto.PrivKey
	pub crypto.PubKey
	admin_priv crypto.PrivKey
	admin_pub crypto.PubKey
	store *secretStore.SecretsStore
}


func NewCrypto(config *config.Config,store *secretStore.SecretsStore) *Crypto {
	c := Crypto{}
	c.store = store
	priv := store.Get("identity_privkey")
	pub := store.Get("identity_pubkey")
	if priv == "" || pub == "" {
		fmt.Println("Generating new Identity Keys....")
		c.genKeys("identity")
	} else {
		privb,_ := b64.StdEncoding.DecodeString(priv)
		pubb,_ := b64.StdEncoding.DecodeString(pub)
		c.priv,_ = crypto.UnmarshalPrivateKey([]byte(privb))
		c.pub,_ = crypto.UnmarshalPublicKey([]byte(pubb))
	}
	admin_priv := store.Get("admin_privkey")
	admin_pub := store.Get("admin_pubkey")
	if admin_priv == "" || admin_pub == "" {
		fmt.Println("Generating new Admin Keys....")
		c.genKeys("admin")
	} else {
		privb,_ := b64.StdEncoding.DecodeString(admin_priv)
		pubb,_ := b64.StdEncoding.DecodeString(admin_pub)
		c.admin_priv,_ = crypto.UnmarshalPrivateKey(privb)
		c.admin_pub,_ = crypto.UnmarshalPublicKey(pubb)
	}
	return &c
}


func (c *Crypto) genKeys(prefix string){
	r := crand.Reader
	priv, pub, err := crypto.GenerateKeyPairWithReader(crypto.RSA, 2048, r)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if prefix == "admin" {
		c.admin_priv = priv
		c.admin_pub = pub
	} else {
		c.priv = priv
		c.pub = pub
	}
	privb,_ := crypto.MarshalPrivateKey(priv)
	pubb,_ := crypto.MarshalPublicKey(pub)
	c.store.Save(prefix + "_privkey",b64.StdEncoding.EncodeToString(privb))
	c.store.Save(prefix + "_pubkey",b64.StdEncoding.EncodeToString(pubb))
}

func (c *Crypto) GetPrivateKey() crypto.PrivKey{
	return c.priv
}

func (c *Crypto) GetPublicKeyString(t string) string {
	return c.store.Get(t + "_pubkey")
}

func (c *Crypto) GetPrivateKeyString(t string) string {
	return c.store.Get(t + "_privkey")
}