package config

import (
	"fmt"
	"github.com/spf13/viper"
	"log"
	"os"
)

func GetConfig (name string) *Config {
	c := &Config{}
	viper.SetConfigName(name)
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/etc/kilavo/configs/")
	viper.AddConfigPath("$HOME/.kilavo/configs")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Panic("Config file not found")
			os.Exit(1)
		} else {
			log.Panic("Error reading config: ", err)
		}
	}
	e := viper.Unmarshal(c)
	if e != nil {
		log.Println("Error reading config File")
		os.Exit(1)
	} else {
		if e := verifyConfig(c); e != nil {
			fmt.Println(e)
			os.Exit(1)
		}
		return c
	}
	return nil
}

func GetCustomConfig (dir,name string) *Config {
	c := &Config{}
	viper.SetConfigName(name)
	viper.SetConfigType("yaml")
	viper.AddConfigPath(dir)
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Panic("Config file not found")
			os.Exit(1)
		} else {
			log.Panic("Error reading config: ", err)
		}
	}
	e := viper.Unmarshal(c)
	if e != nil {
		log.Println("Error reading config File")
		os.Exit(1)
	} else {
		if e := verifyConfig(c); e != nil {
			fmt.Println(e)
			os.Exit(1)
		}
		return c
	}
	return nil
}

func verifyConfig(c *Config) error {
	if _, err := os.Stat(c.Defaults.StorageDirectory); os.IsNotExist(err) {
		e := os.Mkdir(c.Defaults.StorageDirectory, 7777)
		if e != nil {
			return e
		}
	}
	return nil
}