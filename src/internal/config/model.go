package config

type Config struct {
	Defaults struct {
		StorageDirectory string `yaml:"StorageDirectory"`
	} `yaml:"Defaults"`
	Storage struct {
		Secrets struct {
			Bolt struct {
				File string `yaml:"File"`
			} `yaml:"Bolt"`
		} `yaml:"Secrets"`
	} `yaml:"Storage"`
	Networking struct {
		Peers struct {
			Count struct {
				Required   int `yaml:"Required"`
				LowerBound int `yaml:"LowerBound"`
				UpperBound int `yaml:"UpperBound"`
			} `yaml:"Count"`
			Initial struct {
				FromFile  string   `yaml:"FromFile"`
				Endpoints []string `yaml:"Endpoints"`
			} `yaml:"Initial"`
		} `yaml:"Peers"`
		Websockets struct {
			Enable bool `yaml:"Enable"`
			Port   int  `yaml:"Port"`
		} `yaml:"Websockets"`
		WebRTC struct {
			Enable bool `yaml:"Enable"`
			Star string `yaml:"Star"`
		} `yaml:"WebRTC"`
		TCP struct {
			Enable bool `yaml:"Enable"`
			Port   int  `yaml:"Port"`
		} `yaml:"TCP"`
	} `yaml:"Networking"`
	TezosNets []struct {
		Name string `yaml:"Name"`
		Node string `yaml:"Node"`
	} `yaml:"TezosNets"`
}