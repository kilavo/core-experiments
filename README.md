# Kilavo Experiments

This is a Repo for experiments relating to Kilavo,
it currently implements:

* libp2p Networking Layer
  - Websockets
  - TCP
  - WebRTC
* custom libp2p protocol for the Admin Interface
* Federations + Raft consensus algo (WIP)
